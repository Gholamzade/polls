from django.conf.urls import url
from . import views

from . import views

app_name = 'polls'

urlpatterns = [

    # ex: /polls/5/
    # ex: /polls/5/results/
    url(r'^(?P<pk>\d+)/$', views.DetailView.as_view() , name='detail'),
    url(r'^(?P<pk>\d+)/results/', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>\d+)/vote/', views.vote, name='vote'),
    url('', views.IndexView.as_view() , name='index'),


]
    # ex: /polls/5/vote/
